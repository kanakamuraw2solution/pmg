from django.apps import AppConfig


class ProgressmanagementConfig(AppConfig):
    name = 'ProgressManagement'
