# プロジェクト情報
### プロジェクト名
- **日本語名**
	- 進捗管理ツール
- **英名**
	- Progress Management
- **リポジトリ名・略称**
	- pmg

### 言語・フレームワーク
Python・Django
### DB
PostgreSQL
### 開発を行うPC
社内のPC(Windows)

# 開発環境構築手順
## Docker Desktopのインストール
Dockerとは、コンテナと呼ばれるOSの仮想環境を1コマンドでPC上に構築することができ、  
WebサーバやDBサーバなどの各種実行環境の構築を1ファイル上で管理できることで開発者間で開発環境の共有をより簡易に行ったり、  
開発用環境と本番環境の差異を出来る限りなくすことが出来たりするツールです。  
  
Docker Desktopとは、開発用Windows Pc上で実行することが想定されてるGUIのツールです。  
本開発もDocker上で行う想定のためほぼ必須です。（なくても開発自体は行えるはずですが、PythonとかPostgreSQLを自分でインストールすることになるのでしんどいと思います）  
  
[こちら](https://www.docker.com/get-started)にアクセスし、Docker Desktopのインストーラをダウンロードしてください。(ISMS認証済み)  
インストーラを実行し、Docker Desktopをインストールしてください。  

## Visual Studio Codeの準備
VSCode未インストールのかたは[こちら](https://www.microsoft.com/ja-jp/dev/products/code-vs.aspx)からダウンロードしてください。(ISMS認証済み)  
VSCodeを起動し画面左側の「拡張機能」アイコンをクリック、「Remote - Containers」と検索し拡張機能をインストールしてください。（ISMS申請中）  
※Remote - Containersとは、VSCodeからDockerコンテナの内部のフォルダを開くことで、コンテナ内ファイルの編集、実行およびデバッグを行うツールです。  

## gitの準備
git未インストールのかたは[こちら](https://git-for-windows.github.io/)からダウンロードしてください。  (ISMS認証済み)  
当リモートリポジトリに参加し開発を行えるようにするには、最低限以下の手順が必要となります。  

- bitbucketアカウント取得  
- bitbucketにsshキーを登録  
- 当リモートリポジトリをローカルにクローン  

まずはbitbucketのアカウントを取得してください、社内のメールアドレスで問題ないです。  

## bitbucketにsshキーを登録
1. コマンドプロンプト等開き、下記コマンド実行

	`cd C:\Users\{ユーザ名}\（ホームディレクトリにいない場合） `  
	`mkdir .ssh`  
	`cd ./.ssh`   
	`ssh-keygen -t rsa -C {メールアドレス}`

1. ./sshフォルダをファイルエクスプローラなどで開き、テキストファイルを新規作成してファイル名を「config」に変更、テキストエディタで編集し内容を以下に変更する。

	`Host bitbucket.org`  
	`  HostName bitbucket.org`  
	`  IdentityFile ~/.ssh/id_rsa`  
	`  User git`  

1. bitbucketにログインし、画面左下のユーザアイコンクリックし「Personal Settings」をクリック、SSH鍵メニューを選択し「鍵を追加」をクリックするとSSH鍵内容を記入するモーダルが表示される。
1. Label項目に「ssh」と記入、Key項目には、./sshフォルダ内のid_rsa.pubファイルをテキストエディタで開き、内容をコピペ。

## リモートリポジトリをクローン
コマンドプロンプト等開き、適当なフォルダ内で下記コマンド実行（場所はどこでもOKですが、自分は C:\Users\{ユーザ名}\Documents でクローンしました）
```
git clone git@bitbucket.org:kanakamuraw2solution/pmg.git
```
※クローンしてきたフォルダをDockerにマウントするには下記手順が必要になります。
1. Docker DesktopのGUI画面を開き、画面上部の設定アイコンクリック
1. Settingsメニュー中の「Resources > File Sharing」を選択
1. +ボタンクリックし、クローンしてきたpmgフォルダを追加する
Windows上のフォルダを対象にDockerを起動するには、上記設定を行っておかないと起動時にエラーがおきるので注意してください。

## 実行方法
実行前の準備として、中村の個人用フォルダに入っている「local_settings.py」というPythonファイルを、「\pmg\mysite\mysite」フォルダ配下にコピーします。  
 ※「local_settings.py」にはその環境で利用する設定が記述されており、意図的にgitのバージョン管理から外しています。  
DjangoのセキュリティキーとかDBパスワードとか、いわゆるgitのリモートリポジトリに公開したらまずそうな設定を含むので、.gitignoreに記載しているので大丈夫なはずですが万が一にでもうっかりコミットしないよう気を付けてください。  
  
ファイルコピーしましたら、VSCode起動し下記手順実行します。

1. 「Ctrl + shift + p」を押し、メニューから「Remote-Containers: Open Folder in Container」選択
1. pmgフォルダを選択すると、Dockerコンテナがpmgフォルダ上で起動する。
1. 起動したら、VSCodeの上部メニュー「Terminal > New Terminal」を選択し、画面下部のターミナルにて下記コマンド実行

	`cd mysite`  
	`python3 manage.py runserver`

1. ブラウザからlocalhostにアクセスし動作確認

# 開発手順